# Pokecharts

The full Pokémon type charts code and plots to display the strengths and weaknesses of each type.

Note: this charts is for games from 2013 onwards (Pokémon X/Y)