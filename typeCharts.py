import matplotlib.pyplot as plt
import networkx
import networkx as nx


# Functions


def gen_poke_graph(g: networkx.Graph, edges: list, color_map: tuple):
    new_graph = g.copy()
    new_graph.add_edges_from(edges)
    pos = nx.circular_layout(new_graph)
    nx.draw_networkx_nodes(new_graph, pos, node_size=3200, alpha=0.5, node_color=color_map)
    nx.draw_networkx_labels(new_graph, pos)
    nx.draw_networkx_edges(new_graph, pos, node_size=3200, arrowsize=20, arrows=True)

    return new_graph


def gen_edges_effectiveness(chart: dict, effectiveness: float):
    result = []
    keys = list(poke_type_chart.keys())
    for poke_type, values in chart.items():
        for index, value in enumerate(values):
            if value == effectiveness:
                result.append([poke_type, keys[index]])
    return result


# Data

poke_types = ("Normal", "Fighting", "Flying", "Poison", "Ground", "Rock", "Bug",
              "Ghost", "Steel", "Fire", "Water", "Grass", "Electric", "Psychic",
              "Ice", "Dragon", "Dark", "Fairy")
poke_map_color = ("slategrey", "brown", "plum", "purple", "goldenrod",
                  "darkgoldenrod", "olive", "mediumpurple", "silver", "orange",
                  "cornflowerblue", "lawngreen", "yellow", "magenta", "cyan",
                  "blueviolet", "darkslateblue", "pink")

poke_type_chart = {
    # At\Def     Nor, Fir, Wat, Ele, Gra, Ice, Fig, Poi, Gro, Fly, Psy, Bug, Roc, Gho, Dra, Dar, Ste, Fai
    "Normal": [1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 0.5, 0.0, 1.0, 1.0, 0.5, 1.0],
    "Fire": [1.0, 0.5, 0.5, 2.0, 2.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 2.0, 0.5, 1.0, 0.5, 1.0, 2.0, 1.0],
    "Water": [1.0, 2.0, 0.5, 1.0, 0.5, 1.0, 1.0, 1.0, 2.0, 1.0, 1.0, 1.0, 2.0, 1.0, 0.5, 1.0, 1.0, 1.0],
    "Electric": [1.0, 1.0, 2.0, 0.5, 0.5, 1.0, 1.0, 1.0, 0.0, 2.0, 1.0, 1.0, 1.0, 1.0, 0.5, 1.0, 1.0, 1.0],
    "Grass": [1.0, 0.5, 2.0, 1.0, 0.5, 1.0, 1.0, 0.5, 2.0, 0.5, 1.0, 0.5, 2.0, 1.0, 0.5, 1.0, 0.5, 1.0],
    "Ice": [1.0, 0.5, 0.5, 1.0, 2.0, 0.5, 1.0, 1.0, 2.0, 2.0, 1.0, 1.0, 1.0, 1.0, 2.0, 1.0, 0.5, 1.0],
    "Fighting": [2.0, 1.0, 1.0, 1.0, 1.0, 2.0, 1.0, 0.5, 1.0, 0.5, 0.5, 0.5, 2.0, 0.0, 1.0, 2.0, 2.0, 0.5],
    "Poison": [1.0, 1.0, 1.0, 1.0, 2.0, 1.0, 1.0, 0.5, 0.5, 1.0, 1.0, 1.0, 0.5, 0.5, 1.0, 1.0, 0.0, 2.0],
    "Ground": [1.0, 2.0, 1.0, 2.0, 0.5, 1.0, 1.0, 2.0, 1.0, 0.0, 1.0, 0.5, 2.0, 1.0, 1.0, 1.0, 2.0, 1.0],
    "Flying": [1.0, 1.0, 1.0, 0.5, 2.0, 1.0, 2.0, 1.0, 1.0, 1.0, 1.0, 2.0, 0.5, 1.0, 1.0, 1.0, 0.5, 1.0],
    "Psychic": [1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 2.0, 2.0, 1.0, 1.0, 0.5, 1.0, 1.0, 1.0, 1.0, 0.0, 0.5, 1.0],
    "Bug": [1.0, 0.5, 1.0, 1.0, 2.0, 1.0, 0.5, 0.5, 1.0, 0.5, 2.0, 1.0, 1.0, 0.5, 1.0, 2.0, 0.5, 0.5],
    "Rock": [1.0, 2.0, 1.0, 1.0, 1.0, 2.0, 0.5, 1.0, 0.5, 2.0, 1.0, 2.0, 1.0, 1.0, 1.0, 1.0, 0.5, 1.0],
    "Ghost": [0.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 2.0, 1.0, 1.0, 2.0, 1.0, 0.5, 1.0, 1.0],
    "Dragon": [1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 2.0, 1.0, 0.5, 0.0],
    "Dark": [1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 0.5, 1.0, 1.0, 1.0, 2.0, 1.0, 1.0, 2.0, 1.0, 0.5, 1.0, 0.5],
    "Steel": [1.0, 0.5, 0.5, 0.5, 1.0, 2.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 2.0, 1.0, 1.0, 1.0, 0.5, 2.0],
    "Fairy": [1.0, 0.5, 1.0, 1.0, 1.0, 1.0, 2.0, 0.5, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 2.0, 2.0, 0.5, 1.0],
}

# Execution

G = nx.DiGraph()
G.add_nodes_from(poke_types)

# Plot super-effective

plt.figure(1, figsize=(12, 12))
plt.axis('off')
plt.title("Super effective", fontsize=24)

super_effective = gen_edges_effectiveness(poke_type_chart, 2.0)
GSE = gen_poke_graph(G, super_effective, poke_map_color)

# Plot not-very-effective

plt.figure(2, figsize=(12, 12))
plt.axis('off')
plt.title("Not very effective", fontsize=24)

not_very_effective = gen_edges_effectiveness(poke_type_chart, 0.5)
GNV = gen_poke_graph(G, not_very_effective, poke_map_color)

# Plot normal

plt.figure(3, figsize=(12, 12))
plt.axis('off')
plt.title("Normal", fontsize=24)

normal = gen_edges_effectiveness(poke_type_chart, 1.0)
GNO = gen_poke_graph(G, normal, poke_map_color)

# Plot no-effective

plt.figure(4, figsize=(12, 12))
plt.axis('off')
plt.title("No effect", fontsize=24)

no_effective = gen_edges_effectiveness(poke_type_chart, 0.0)
GNE = gen_poke_graph(G, no_effective, poke_map_color)

plt.show()
